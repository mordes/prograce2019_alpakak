import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;



 
public class Main {

    public static void main(String[] args) {
        // Nem volt idő felolvasni ... 
        double values [][] = {
            {21.404911367956082,27.66498865361914,34.19772602416753,34.4012825991832,33.67936422421228,27.73372063072164,26.668021496510647,30.50972851247457},{20.791885532482553,29.10321336116999,31.21597273544209,37.1844075065794,33.605319622311065,27.39304331114618,26.79420155958944,34.45893542104819},{24.089313036789612,31.92318707943194,32.366437707966206,38.35177599834523,35.9313748630342,28.074500028584048,27.935763116808932,39.71151091036922},{27.881439555285983,31.37142880449459,32.45457546985326,37.97973601122672,36.83219411086911,27.717736405448534,29.546095387367707,44.489465763222306},{28.800152776899616,31.027624502441405,35.01946181797907,38.20236328956638,38.29271781933399,31.03429387298837,30.40141696074368,47.13053666509675},{28.704897937667873,32.6506870738734,35.77453147217146,38.895638060436895,36.619413684110214,32.99800484051353,32.20156265181219,46.821800411163416},{27.807894094011388,32.77913138032368,39.6118595635247,40.83986759867444,35.959409968834635,35.53085766805357,35.083786625770855,50.75633918145083}};
        double time;

        /*
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("out.txt")));
            time = Double.parseDouble(br.readLine());
            ArrayList<Double []> a = new ArrayList<>();
            while (br.ready()) {
   
                double value [] = new double [8];
                
                String s = br.readLine();
                String [] ss = s.split(",");
                for (int i = 0; i < ss.length; i++) {
                    value[i] = Double.parseDouble(ss[i]);
                }


            }
            br.close();

        }

        catch (Exception e) {
            e.printStackTrace();
        }
        */

        int jsonLength = values.length;



        String json = "";
        json = "{\"FrameCount\": " + jsonLength + ",";
        json += "\"Frames\":[";


        ArrayList<WaveColumn> cols;

        for (int i = 0; i < values.length; i++) {
            cols  = new ArrayList<WaveColumn>();
            json += "{\"SaveBlock\":[";
            int[][] colors = new int[512][3];

            for (int j = 0; j < values[i].length; j++) {
                cols.add(new WaveColumn(values[i][j]));
            }

            int [] color = new int[8];
            int s = 0;
            for (WaveColumn w : cols) {
                color[s] = w.getColLevel();
            }


            json += "";

            int ss = 0;
            for (int k = 0; k < 512; k++) {
                if (k % 8 == 0) {
                    k += 56;
                    ss = 0;
                    for (int kk = 0; kk < 56 * 3; kk++) {
                        json += "0,";
                        
                    }
                }
                
                if (color[ss] > 0) {
                    json += "255, 255, 0,";
                    color[ss]--; 
                }
                ss++;   
            }
            json = json.substring(0, json.length() - 1);
            json += "],\"time\":1},";
        }
        json = json.substring(0, json.length() - 1);

        json += "]}";


        System.out.println(json);
    }


}