import java.util.Random;

public class WaveColumn {

    private double value;
    private static int colIndex = 0;
    private int currentColIndex;
    private int rgb[];
    private static final double duration = 0.6f;
    private int colLevel;


    public WaveColumn(double value) {
        this.value = value;
        currentColIndex = colIndex;
        colIndex = colIndex + 1 == 8 ? 0 : colIndex + 1;
        


        // Fill colors
        rgb = new int[3];
        for (int i = 0; i < rgb.length; i++) {
            //rgb[i] = generateRgb();
            rgb[i] = 255;
        }
        rgb[rgb.length - 1] = 0;
        

        colLevel = convertValueToColValue(value, 0, 255, 0, 8);
        System.out.println("a");
    }

    public int getColLevel() {
        return colLevel;
    }

    public int[] getRgbC() {
        return this.rgb;
    }

    private String getRgb() {
        String s  = "";
        for (int i = 0; i < this.rgb.length; i++) {
            s += this.rgb[i] + ", ";
        }
        return s;
    }

    private int generateRgb() {
        return new Random().nextInt(256);

    }

    private int convertValueToColValue(double value, int leftMin, int leftMax, int rightMin, int rightMax) {
        int leftSpan = leftMax - leftMin;
        int rightSpan = rightMax - rightMin;

        double valueScaled = (double)(value - leftMin) / (double)(leftSpan);

        return (int)(rightMin + valueScaled * rightSpan);
    }

    @Override
    public String toString() {
        String s = this.value + " - " + currentColIndex + " - (";
        for (int i = 0; i < this.rgb.length; i++) {
            s += this.rgb[i] + ", ";
        }
        s += ")";
        s += duration + " - " + colLevel;
        return s;
    }

    

}